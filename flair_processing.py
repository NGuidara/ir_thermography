# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 19:03:39 2020

@author: Noah Guidara
"""

import cv2


#Sobel filter: Params = [Data type, dx, dy, kernel size]
#Blur amount must be odd and positive
def get_raw_sobel(f_img, blur_amt, params):
    raw_img = f_img.img
    raw_sob = cv2.GaussianBlur(raw_img, (blur_amt,blur_amt), 0)
    raw_sob = cv2.Sobel(raw_sob, params[0], params[1], params[2], params[3])
    return raw_sob

#Scharr filter: Params = [Data type, dx, dy]
#Blur amount must be odd and positive
#One of dx or dy must be zero
def get_raw_scharr(f_img, blur_amt, params):
    raw_img = f_img.img
    raw_sch = cv2.GaussianBlur(raw_img, (blur_amt, blur_amt), 0)
    raw_sch = cv2.Scharr(raw_sch, params[0], params[1], params[2])
    return raw_sch

#Laplacian filter: Params = [Data type]
#blur amount must be odd and positive
def get_raw_laplacian(f_img, blur_amt, params):
    raw_img = f_img.img
    raw_lap = cv2.GaussianBlur(raw_img, (blur_amt, blur_amt), 0)
    raw_lap = cv2.Laplacian(raw_lap, params[0])
    return raw_lap

#Custom filter: No params but you need to provide a kernel
def get_raw_custom_filter(f_img, blur_amt, kernel):
    raw_img = f_img.img
    raw_cus = cv2.GaussianBlur(raw_img, (blur_amt, blur_amt), 0)
    raw_cus = cv2.filter2D(raw_cus, -1, kernel)
    return raw_cus