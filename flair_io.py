# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 18:16:59 2020

@author: Noah Guidara
"""

import flair_errors as ferrors
from pathlib import Path
import flair_directory as fdir
import flair_image as fimg
import cv2

acceptable_image_formats = ['.tiff']
predicates = []
loaded_images = []
saved_images = []
directory_chain = []

def directory_exists(directory):
    return Path(directory).is_dir

def file_exits(src):
    return Path(src).is_file

def load_image(directory, name):
    if directory_exists(directory):
        #Update directory chain
        directory_chain.append(directory)
        
        flair_img = fimg.flair_image(directory, name)
        loaded_images.append(flair_img)
        return flair_img
    else:
        #TODO: Error here
        pass

def save_image(img, directory, name):
    if directory_exists(directory):
        #Update directory chain
        directory_chain.append(directory)
        
        #TODO: Figure out exactly how to save the image
    else:
        #TODO: Error here
        pass

def change_image_directory(img, directory):
    if directory_exists(directory):
        #Update directory chain
        directory_chain.append(directory)
        
        img.update_directory(directory)
    else:
        #TODO: Error here
        pass