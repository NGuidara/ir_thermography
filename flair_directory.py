# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 18:16:59 2020

@author: Noah Guidara
"""

import image_error
from pathlib import Path

class flair_directory:
    
    def __init__(self, src):
        #Sanity check on source
        if Path(src).is_file:
            #Ending is a slash
            if src(len(src) - 1) != '/':
                src += '/'
            self.src = src
        else:
            #Bad directory
            raise image_error.InvalidDirectoryError("if Path(src).is_file:",
                            "Path does not exist or directory is invalid.")
        #todo: get number of image files and img files