# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 19:56:07 2020

@author: Noah Guidara
"""

import matplotlib.pyplot as plt

to_be_plotted = []

#TODO: Define what args are default
def reset_plot_args():
    pass

def plot_whole_image(f_img):
    raw_img = f_img.img
    plt.imshow(raw_img)
    plt.show()

def lineout_by_row(f_img, ind_row, ind_start_col = None, ind_end_col = None):
    if ind_start_col == None and ind_end_col == None:    
        plt.plot(f_img[ind_row])
        plt.show()
    else:
        #Interval argument errors will be on the end user, not within this function.
        if ind_start_col == None or ind_end_col == None:
            #Only one is none, error
            #TODO: Implement proper error
            return
        plt.plot(f_img[ind_row,ind_start_col:ind_end_col])
        plt.show()

def lineout_by_col(f_img, ind_col, ind_start_row = None, ind_end_row = None):
    if ind_start_row == None and ind_end_row == None:     
        plt.plot(f_img[:,ind_col])
        plt.show()
    else:
        if ind_start_row == None or ind_end_row == None:
            #Only one is none, error
            #TODO: Implement proper error
            return
        plt.plot(f_img[ind_start_row:ind_end_row, ind_col])
        plt.show()