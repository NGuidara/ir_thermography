# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 15:47:49 2020

@author: Noah Guidara
"""

class FlairError(Exception):
    #Base error for FLAIR Lab.
    pass

class InvalidDirectoryError(FlairError):
    #Raised when the directory given is not correct for any reason.
    def __init__(self, expression, message):
        self.expression = expression
        self.message = message

class InvalidExtensionError(FlairError):
    #Raised when the extension does not meet the types expected.
    def __init__(self, expression, expected_types, received_type, message):
        self.expression = expression
        self.message = message
        self.conflict = (expected_types, received_type)

class ImageError(FlairError):
    pass

class ImageDimensionError(ImageError):
    #Raised when trying to blanket apply operations on differently sized images.
    def __init__(self, image, expression, expected_dim, received_dim, message):
        self.expression = expression
        self.message = message
        self.image = image
        self.conflict = (expected_dim, received_dim)
        
class ImageColorError(ImageError):
    #Raised when trying to mix operations on something like RGB vs Grayscale images.
    def __init__(self, image, expression, expected_color_type, received_color_type, message):
        self.expression = expression
        self.message = message
        self.image = image
        self.conflict = (expected_color_type, received_color_type)
        
class ImageOutOfBoundsError(ImageError):
    #Raised when trying to apply operations outside of an image's dimensions.
    def __init__(self, image, expression, expected_coords, received_coords, message):
        self.expression = expression
        self.message = message
        self.image = image
        #Expected coords will be ((min_row, min_col), (max_row, max_col))
        self.conflict = (expected_coords, received_coords)