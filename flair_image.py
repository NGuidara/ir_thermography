# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 18:16:59 2020

@author: Noah Guidara
"""

import cv2

class flair_image:
    #Wrapper class for an image: Should have raw data, image type, image size etc.
    def __init__(self, f_dir, name):
        src = f_dir.src + name
        self.dir = f_dir
        self.name = name
        self.img = cv2.imread(src, -1)
        #Median blur applied to filter hot pixels
        self.img = cv2.medianBlur(self.img, 5)
        self.data_type = type(self.img[0,0])
        self.shape = self.img.shape
        self.dim_row = self.shape[0]
        self.dim_col = self.shape[1]
        
        #Shape for grayscale images only has numrows and numcols.
        if len(self.shape) == 2:
            self.grayscale = True
        else:
            self.grayscale = False
        #NOTE: img[x,y,0] = Blue, img[x,y,1] = Green, img[x,y,2] = Red

#TODO: Sanity checks for the updates.
    def update_image(self, new_img):
        prev_img = self.img
        self.img = new_img
        return prev_img
    
    def update_directory(self, new_dir):
        prev_dir = self.dir
        self.dir = new_dir
        return prev_dir
    
#TODO: Make sure this doesn't mess up IO on saving/loading etc.
    def update_name(self, new_name):
        prev_name = self.name
        self.name = new_name
        return prev_name

#Intervals should be a 2 member list [start, end] indices.
    def get_sub_image(self, interval_row, interval_col):
        sub_img = self.img[interval_row[0]:interval_row[1], interval_col[0]:interval_col[1]]
        return sub_img
    
    def get_row(self, ind_row):
        row = self.img[ind_row]
        return row
    
    def get_col(self, ind_col):
        col = self.img[:,ind_col]
        return col
    
#Note: While lineouts have optional intervals, sub row / sub col must have intervals.
    def get_sub_row(self, ind_row, ind_start_col, ind_end_col):
        sub_row = self.img[ind_row, ind_start_col:ind_end_col]
        return sub_row
    
    def get_sub_col(self, ind_col, ind_start_row, ind_end_row):
        sub_col = self.img[ind_start_row:ind_end_row, ind_col]
        return sub_col